var express = require('express');
var router = express.Router();

var User = require('../dsnetwork/User');
var Coin = require('../dsnetwork/Coin');
/* GET home page. */
router.get('/', function(req, res, next) {
	res.send('homepage')
});

router.get('/users', function(req, res, next) {
	User.getAll(function (users) {
		res.send(users)
	})
});

router.get('/coins', function(req, res, next) {
	Coin.getAll(function (coins) {
		console.log(coins.length);
		res.send(coins)
	})
});

router.get('/user', function(req, res, next) {
	User.getAll(function (users) {
		res.send(users)
	})
});

router.post('/user', function(req, res, next) {
	User.addUser(req.body, function (user) {
		res.send(user)
	})
});

router.post('/addCoins', function(req, res, next) {
	Coin.addCoins(req.body, function (new_coins) {
		res.send(new_coins)
	})
});

router.post('/transferCoins', function(req, res, next) {
	Coin.transferCoins(req.body, function (msg) {
		res.send(msg)
	})
});

module.exports = router;
