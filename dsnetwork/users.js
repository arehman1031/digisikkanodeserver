const BusinessNetworkConnection = require('composer-client').BusinessNetworkConnection;
conn = new BusinessNetworkConnection();
let cardName = 'admin@dsnetwork'

function generateId() {
	return Math.random().toString(36).substr(5)
}

conn.connect(cardName)
.then(function (dsNetworkDefinition) {
	let User = conn.getParticipantRegistry('org.fyp.ds.User')

	User.then(function (userRegistry) {
		return userRegistry.getAll();
	})
	.then(function (users) {
		console.log(users);
		return ;
		let next_user_id = users.length + 1

		let factory = dsNetworkDefinition.getFactory()
		user = factory.newResource('org.fyp.ds', 'User',''+next_user_id)
		user.name = 'AR_'+next_user_id
		user.coins = []

		User.then(function (userRegistry) {
			userRegistry.add(user)
			.then(function () {
				conn.disconnect()
				.then(function () {
					console.log('disconnected');
				})
				.catch(function (e) {
					console.log(e);
				})
			})
		})
	})

})
