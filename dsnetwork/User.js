const BusinessNetworkConnection = require('composer-client').BusinessNetworkConnection;
const bConn = new BusinessNetworkConnection();
var common = require('../common')

const cardName = 'admin@dsnetwork'

let conn = bConn.connect(cardName)

module.exports = {
	addUser: function (param, fn) {
		conn.then(function (nd) {
			User = bConn.getParticipantRegistry('org.fyp.ds.User')
			User.then(function (userRegistry) {
				return userRegistry.getAll();
			})
			.then(function (users) {
				let factory = nd.getFactory()
				next_user_id = common.generateId()
				user = factory.newResource('org.fyp.ds', 'User', next_user_id)
				user.name = param.name
				user.email = param.email
				user.password = param.password
				user.coins = []
				User.then(function (userRegistry) {
					userRegistry.add(user)
					.then(function () {
						fn(user)
					})
				})
			})
			return nd;
		})
	},
	getUser: function (userId, fn) {
		conn.then(function (nd) {
			User = bConn.getParticipantRegistry('org.fyp.ds.User')
			User.then(function (userRegistry) {
				userRegistry.get(userId)
				.then(function (user) {
					fn(user)
				})
			})

			return nd;
		})
	},
	getUserByEmail: function (email, fn) {
		conn.then(function (nd) {
			// const query = bConn.buildQuery('SELECT org.fyp.ds.User WHERE (email == _$inputValue)');
			// const assets = bConn.query(query, { inputValue: email })
			// console.log(assets);
		})
	},
	getAll: function (fn) {
		conn.then(function (nd) {
			User = bConn.getParticipantRegistry('org.fyp.ds.User')
			User.then(function (userRegistry) {
				userRegistry.getAll()
				.then(function (users) {
					fn(users)
				})
			})
			return nd;
		})
	}
}
