const BusinessNetworkConnection = require('composer-client').BusinessNetworkConnection;
const common = require('../common')
const bConn = new BusinessNetworkConnection();
const bcrypt = require('bcrypt');

const cardName = 'admin@dsnetwork'
const conn = bConn.connect(cardName)

function updateCoinsOwner(coinRegistry, coins, coin_no, owner_id) {
	return new Promise(function(resolve, reject) {
		if(coin_no < coins.length) {
			coin_id = coins[coin_no].getIdentifier()
			coinRegistry.get(coin_id)
			.then(function (coin) {
				coin.owner = factory.newRelationship('org.fyp.ds', 'User', owner_id)
				return coinRegistry.update(coin)
				.then(function () {
					return updateCoinsOwner(coinRegistry, coins, coin_no+1, owner_id)
					.then(function () {
						resolve()
					})
				})
			})

		} else {
			resolve()
		}
	});
}

module.exports = {
	transferCoins: function (param, fn) {
		conn.then(function (nd) {
			let User = bConn.getParticipantRegistry('org.fyp.ds.User')
			let Coin = bConn.getAssetRegistry('org.fyp.ds.Coin')
			User.then(function (userRegistry) {
				userRegistry.get(param.user1)
				.then(function (user1) {
					userRegistry.get(param.user2)
					.then(function (user2) {
						if (user1.coins.length >= param.noOfCoins) {
							factory = nd.getFactory()
							coins = user1.coins.splice(0, param.noOfCoins)
							user2.coins = user2.coins.concat(coins)
							Coin.then(function (coinRegistry) {
								userRegistry.update(user1)
								.then(function () {
									userRegistry.update(user2)
									.then(function () {
										updateCoinsOwner(coinRegistry, coins, 0, user2.userId)
										.then(function () {
											fn({msg: 'done'})
										})
									})
								})
							})
						} else {
							fn({msg: 'not enough coins.'})
						}
					})
					.catch(function (e) {
						console.log(e);
						fn({msg: e})
					})
				})
				.catch(function (e) {
					console.log(e);
					fn({msg: e})
				})
			})
			return nd;
		})
	},
	addCoins: function (param, fn) {
		conn.then(function (nd) {
			let User = bConn.getParticipantRegistry('org.fyp.ds.User')
			let Coin = bConn.getAssetRegistry('org.fyp.ds.Coin')
			User.then(function (userRegistry) {
				userRegistry.get(param.userId)
				.then(function (user) {
					Coin.then(function (coinRegistry) {
							let next_coin_id = common.generateId()
							let new_coins = []
							let factory = nd.getFactory()
							for (var i = 0; i < param.noOfCoins; i++) {
								coin = factory.newResource('org.fyp.ds', 'Coin', ''+next_coin_id)
								// coin.owner = user.getIdentifier()
								coin.owner = factory.newRelationship( 'org.fyp.ds', 'User', user.userId )
								user.coins.push(factory.newRelationship( 'org.fyp.ds', 'Coin', coin.coinId ))
								new_coins.push(coin)
								next_coin_id = common.generateId()
							}
							Coin.then(function (coinRegistry) {
								coinRegistry.addAll(new_coins)
								.then(function () {
									userRegistry.update(user)
									.then(function () {
										fn(new_coins)
									})
								})
							})
					})
				})
				.catch(function () {
					fn([])
				})
			})
			return nd;
		})
	},
	getAll: function (fn) {
		conn.then(function (nd) {
			let Coin = bConn.getAssetRegistry('org.fyp.ds.Coin')
			Coin.then(function (coinRegistry) {
				coinRegistry.getAll()
				.then(function (coins) {
					fn(coins)
				})
			})
			return nd;
		})
	}
}
