const BusinessNetworkConnection = require('composer-client').BusinessNetworkConnection;
conn = new BusinessNetworkConnection();
let cardName = 'admin@dsnetwork'

let user_id = '1'

function generateId() {
	return Math.random().toString(36).substr(5)
}

conn.connect(cardName)
.then(function (dsNetworkDefinition) {

	let User = conn.getParticipantRegistry('org.fyp.ds.User')
	let Coin = conn.getAssetRegistry('org.fyp.ds.Coin')
	User.then(function (userRegistry) {
		return userRegistry.get(user_id);
	})
	.then(function (user) {
		Coin.then(function (coinRegistry) {
			return coinRegistry.getAll();
		})
		.then(function (coins) {
			console.log(coins);
			return ;
			let next_coin_id = coins.length + 1
			let new_coins = []
			let factory = dsNetworkDefinition.getFactory()
			for (var i = 0; i < 5; i++) {
				coin = factory.newResource('org.fyp.ds', 'Coin', ''+next_coin_id)
				// coin.owner = user.getIdentifier()
				coin.owner = factory.newRelationship( 'org.fyp.ds', 'User', user.userId )
				user.coins.push(factory.newRelationship( 'org.fyp.ds', 'Coin', coin.coinId ))
				new_coins.push(coin)
				next_coin_id++
			}
			Coin.then(function (coinRegistry) {
				coinRegistry.addAll(new_coins)
				.then(function () {
					User.then(function (userRegistry) {
						userRegistry.update(user)
						.then(function () {
							conn.disconnect()
							.then(function () {
								console.log('disconnected...');
							})
						})
					})
				})
			})
		})
	})
	.catch(function (e) {
		console.log(e);
	})
})
