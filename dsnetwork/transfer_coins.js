const BusinessNetworkConnection = require('composer-client').BusinessNetworkConnection;
conn = new BusinessNetworkConnection();
let cardName = 'admin@dsnetwork'
let user_1 = '1'
let user_2 = '2'
let noOfCoins = 2

function generateId() {
	return Math.random().toString(36).substr(5)
}

function updateCoin(Coin, coin_id, owner_id) {
	return Coin.then(function (coinRegistry) {
		return coinRegistry.get(coin_id)
		.then(function (coin) {
			coin.owner = factory.newRelationship('org.fyp.ds', 'User', owner_id)
			coinRegistry.update(coin)
		})
	})
}

function updateCoinsOwner(coinRegistry, coins, coin_no) {
	return new Promise(function(resolve, reject) {
		if (coin_no < coins.length) {
			coin_id = coins[coin_no].getIdentifier()
			coinRegistry.get(coin_id)
			.then(function () {
				coin_no++
				updateCoinsOwner(Coin, coins, coin_no)
				.then(function () {
					resolve()
				})
			})
		} else {
			resolve('done')
		}
	});
}

conn.connect(cardName)
.then(function (dsNetworkDefinition) {
	let User = conn.getParticipantRegistry('org.fyp.ds.User')
	let Coin = conn.getAssetRegistry('org.fyp.ds.Coin')

	User.then(function (userRegistry) {
		userRegistry.get(user_1)
		.then(function (user1) {
			userRegistry.get(user_2)
			.then(function (user2) {
				if (user1.coins.length >= noOfCoins) {
					console.log('enough coins...');
					factory = dsNetworkDefinition.getFactory()
					coins = user1.coins.splice(0, noOfCoins)
					for (var cc = 0; cc < coins.length; cc++) {
						coin_id = coins[cc].getIdentifier()
						console.log('coin_id', coin_id);
						// updateCoin(Coin, coin_id, user2.userId)
					}
					user2.coins = user2.coins.concat(coins)

					userRegistry.update(user1)
					.then(function () {
						userRegistry.update(user2)
						.then(function () {
							conn.disconnect()
							.then(function () {
								console.log('disconnected...');
							})
							.catch(function (e) {
								console.log(e);
							})
						})

					})
					.catch(function (e) {
						console.log('occur in user update...');
						console.log(e);
					})
				}
			})
			.catch(function (e) {
				console.log(e);
			})
		})
		.catch(function (e) {
			console.log(e);
		})
	})
})
